//
//  FeedCell.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/25/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import UIKit

class FeedCell: BaseCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var videos :[Video]?
    
    var homeController: HomeController?  
    
    func fetchVideos() {
        
        
        APIService.SharedInstance.fetchFeedFor(View: .home) { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
        
        
    }
    
    
    lazy var collectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    
    }()
    
    let vID = "vID"
   

    override func setupViews() {
        super.setupViews()
        addSubview(collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        
        collectionView.register(VideoCell.self, forCellWithReuseIdentifier: vID)
        fetchVideos()
    }
    
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return videos?.count ?? 0
        }
    
         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: vID, for: indexPath) as! VideoCell
            cell.video = videos?[indexPath.row]
           
            return cell
        }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sampleVideo:URL = NSURL(string: "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4") as! URL
        let urls: [URL] = [sampleVideo]
        if !YTFPlayer.isOpen() {
            
            
            YTFPlayer.initYTF(urls, tableCellNibName: vID, delegate: self, dataSource: self)
            YTFPlayer.showYTFView(self.homeController!)
        } else {
            YTFPlayer.finishYTFView(true)
            YTFPlayer.initYTF(urls, tableCellNibName: vID, delegate: self, dataSource: self)
            YTFPlayer.showYTFView(self.homeController!)
            
        }
        
    }


        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
            let height = (self.frame.width - 16 - 16) * 9/16
            return CGSize(width: self.frame.width, height: height + 16 + 88)
        }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
    
    

}
