//
//  APIService.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/21/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import Foundation

class APIService :NSObject {

static let SharedInstance = APIService()
    
        
    func fetchFeedFor(View:LoadingView, completion:@escaping ([Video])->()){
        let url = NSURL(string: "https://s3-us-west-2.amazonaws.com/youtubeassets/\(View).json")
        URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            
            if error != nil {
                print(error ?? "")
                return
            }
            
            do {
                
                if let unwrappedData = data , let jsonDict = try JSONSerialization.jsonObject(with: unwrappedData, options: .mutableContainers) as? [[String: AnyObject]] {
                 
                    DispatchQueue.main.async {
                        completion(jsonDict.map({return Video(dictionary: $0)}))
                    }
                }
            } catch let jsonError {
                print(jsonError)
            }
            
            
            
            }.resume()
        
    }

    

}
