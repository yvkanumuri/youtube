//
//  settingCell.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/17/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import UIKit

class settingCell: BaseCell {
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white
            
            nameLabel.textColor = isHighlighted ? UIColor.white : UIColor.black
            
            settingImage.tintColor = isHighlighted ? UIColor.white : UIColor.darkGray
        }
    }
    
    
    override func setupViews() {
        super.setupViews()
        addSubview(nameLabel)
        addSubview(settingImage)
        addConstraintsWithFormat("H:|-16-[v0(30)]-8-[v1]-16-|", views:settingImage,nameLabel)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
         addConstraintsWithFormat("V:[v0(30)]", views: settingImage)
        
        addConstraint(NSLayoutConstraint(item: settingImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    var setting: Setting? {
        didSet {
            nameLabel.text = setting?.name.rawValue
            
            if let imageName = setting?.imageName {
                settingImage.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
                settingImage.tintColor = UIColor.darkGray
            }
        }
    }
    
    
    let nameLabel:UILabel = {
    
        let lable = UILabel()
        lable.text = "yesh"
        lable.font = UIFont.systemFont(ofSize: 13)
        return lable
    
    }()
    
    
    let settingImage:UIImageView = {
    let image = UIImageView()
        image.image = UIImage(named: "settings")
        image.contentMode = .scaleAspectFill

     return image
    
    }()
    
}
