//
//  menuBar.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/14/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import UIKit


class menuBar: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    lazy var collectionView:UICollectionView = {
    
    let layout = UICollectionViewFlowLayout()
        
       let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(230, green: 32, blue: 31)
        cv.dataSource = self
        cv.delegate = self
        return cv
    
    }()
    
    
    let cellID = "cellID"
    let imageNames = ["home", "trending", "subscriptions", "account"]
    var horizonBarLeftConstraint:NSLayoutConstraint?
    var homeController:HomeController?
    override init(frame: CGRect){
        
        super.init(frame: frame)
        
       
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellID)
        
        addSubview(collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
        let selectedPath = NSIndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedPath as IndexPath, animated: false, scrollPosition: [])
        
        setupHorizontalSlider()

    }
    
    func setupHorizontalSlider(){
    
        let barView = UIView()
        barView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        barView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(barView)
        
        horizonBarLeftConstraint = barView.leftAnchor.constraint(equalTo: self.leftAnchor)
        
        horizonBarLeftConstraint?.isActive = true
        barView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        barView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25).isActive = true
        barView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MenuCell
        cell.imageView.image = UIImage(named: "\(imageNames[indexPath.row])")?.withRenderingMode(.alwaysTemplate)
        cell.tintColor = UIColor.rgb(91, green: 14, blue: 13)
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 4, height: frame.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let x = CGFloat(indexPath.item) *  frame.width / 4
        horizonBarLeftConstraint?.constant = x
        homeController?.scroolToMenuAtIndexPath(indexpath: indexPath.item)
//        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: { 
//            self.layoutIfNeeded()
//        }, completion: nil)
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    
    
}


class MenuCell:BaseCell  {
    
    
    var imageView:UIImageView = {
    
    let iv = UIImageView()
     iv.image = UIImage(named: "home")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor.rgb(91, green: 14, blue: 13)
        return iv
    
    }()
    
    override var isHighlighted: Bool {
    
        didSet {
            imageView.tintColor = isHighlighted ? UIColor.white : UIColor.rgb(91, green: 14, blue: 13)
        }
    }
    
    
    override var isSelected: Bool {
        
        didSet {
            imageView.tintColor = isSelected ? UIColor.white : UIColor.rgb(91, green: 14, blue: 13)
        }
    }

   
    
    override func setupViews(){
    super.setupViews()
   addSubview(imageView)
    addConstraintsWithFormat("H:[v0(28)]", views: imageView)
           addConstraintsWithFormat("V:[v0(28)]", views: imageView)
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraintsWithFormat("V:[v0(28)]", views: imageView)
        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
}


















