//
//  TrendingCell.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/27/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import UIKit


enum LoadingView:String {
 
    case home = "home"
    case trending = "trending"
    case subscriptions = "subscriptions"
    
}

class TrendingCell: FeedCell {

    override func fetchVideos() {
        
        
        APIService.SharedInstance.fetchFeedFor(View: .trending) { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
        }
}

class SubscriptionCell: FeedCell {
    
    override func fetchVideos() {
        
        APIService.SharedInstance.fetchFeedFor(View: .subscriptions) { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
        }
    }
    
}
