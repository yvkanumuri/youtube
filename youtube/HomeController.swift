//
//  ViewController.swift
//  youtube
//
//  Created by Yeswanth varma Kanumuri on 2/12/17.
//  Copyright © 2017 Yeswanth varma Kanumuri. All rights reserved.
//

import UIKit

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let cellID = "cellID"
    let TRENDINGCELLID = "TRENDINGCELLID"
    let SubscriptionCellID  = "SubscriptionCellID"
    let titles = ["  Home", "  Trending", "  Subscriptions","  Account"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isTranslucent = false
       
        //fetchVideos()
        let titleLabel = UILabel(frame:CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height ))
        titleLabel.text = "  HOME"
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
        setupCollectionView()
        setupMenuBar()
        setupNavBarButtons()
        
    }
    
    lazy var MenuBar:menuBar = {
    
        let mb = menuBar()
       mb.homeController = self
        return mb
    
    }()
    
    
    func setupCollectionView(){
        
        if let flowlayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
        
        flowlayout.scrollDirection = .horizontal
            flowlayout.minimumLineSpacing = 0
        }
    
        collectionView?.backgroundColor = UIColor.white
        //collectionView?.register(VideoCell.self, forCellWithReuseIdentifier: "cellID")
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: cellID)
        collectionView?.register(TrendingCell.self, forCellWithReuseIdentifier: TRENDINGCELLID)
        collectionView?.register(SubscriptionCell.self, forCellWithReuseIdentifier: SubscriptionCellID)
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.isPagingEnabled = true

    }
    
    private func setupNavBarButtons(){
        let searchImage = UIImage(named: "search_icon")?.withRenderingMode(.alwaysOriginal)
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        
        let navmorehImage = UIImage(named: "nav_more_icon")?.withRenderingMode(.alwaysOriginal)
        let moreBarButtonItem = UIBarButtonItem(image: navmorehImage, style: .plain, target: self, action: #selector(handleMore))
    navigationItem.rightBarButtonItems = [moreBarButtonItem,searchBarButtonItem]
    }
    
    
   lazy var SettingsLauncher :settingsLauncher = {
    let launcher = settingsLauncher()
        launcher.homeCOntroller = self
        return launcher
    
    }()
    
    func handleSearch(){
   
    }
    
    func handleMore(){
        //SettingsLauncher.homeCOntroller = self
     SettingsLauncher.showSettings()
        
    
    }
    
    
    func showControllerForSettings(setting:Setting){
    
        let dummySettingsViewController = UIViewController()
        dummySettingsViewController.view.backgroundColor = UIColor.white
        
        dummySettingsViewController.navigationItem.title = setting.name.rawValue
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        navigationController?.pushViewController(dummySettingsViewController, animated: true)
    
    }
    
    func scroolToMenuAtIndexPath(indexpath:Int){
    
        let indexpathh = NSIndexPath(item: indexpath, section: 0)
        collectionView?.scrollToItem(at: indexpathh as IndexPath, at:[], animated: true)
        setNavTitle(index: indexpath)
    
    }
    
    private func setNavTitle(index:Int){
    
        if let titleLabel = navigationItem.titleView as? UILabel {
            
            titleLabel.text = " " + titles[index]
            
        }
    
    }
    
    
    private func setupMenuBar(){
        
        navigationController?.hidesBarsOnSwipe = true
        
        
        let redview = UIView()
        redview.backgroundColor = UIColor.rgb(230, green: 32, blue: 31)
        view.addSubview(redview)
        view.addConstraintsWithFormat("H:|[v0]|", views: redview)
        view.addConstraintsWithFormat("V:[v0(50)]", views: redview)
    
    view.addSubview(MenuBar)
     view.addConstraintsWithFormat("H:|[v0]|", views: MenuBar)
        view.addConstraintsWithFormat("V:[v0(50)]", views: MenuBar)
        MenuBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        MenuBar.horizonBarLeftConstraint?.constant = scrollView.contentOffset.x / 4
    }
  
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.move().x / view.frame.width
        let indexpath = NSIndexPath(item: Int(index), section: 0)
        MenuBar.collectionView.selectItem(at: indexpath as IndexPath, animated: true, scrollPosition: [])
        
        setNavTitle(index: Int(index))
        
       
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var identifire:String = ""
        switch indexPath.item {
        case 0:
            identifire = cellID
        case 1:
             identifire = TRENDINGCELLID
        case 2:
             identifire = SubscriptionCellID
        default:
             identifire = cellID
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifire, for: indexPath) as! FeedCell
        cell.homeController = self
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 50)
    }
    
}














